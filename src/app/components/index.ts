
import { HeaderComponent } from './header/header.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { FooterComponent } from './footer/footer.component';

export const components: any[] = [
  HeaderComponent,
  CalculatorComponent,
  FooterComponent
];

export * from './header/header.component';
export * from './calculator/calculator.component';
export * from './footer/footer.component';
