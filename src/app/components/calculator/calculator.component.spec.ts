import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';

import { CurrencyService } from '@scrooge/services/currency.service';

describe('CalculatorComponent', () => {
  let fixture: ComponentFixture<CalculatorComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ CurrencyService ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
  });
});
