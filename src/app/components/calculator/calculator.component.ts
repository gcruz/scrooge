import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyService } from '@scrooge/services/currency.service';
import { environment as env } from '@scrooge/env/environment';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  constructor(private fb: FormBuilder, private service: CurrencyService) { }

  private myForm: FormGroup;
  private timeRequest: Date;
  private rates: object;
  private eurResult: number = 0;
  private wait: number = 10; // minutes xD

  ngOnInit() {
    this.myForm = this.createCompanyForm();
  }

  createCompanyForm(): FormGroup {
    return this.fb.group({
      usd: [0, [
        Validators.required
      ]]
    });
  }

  checkWaitTime(): boolean {
    return ((+new Date() - +this.timeRequest) / 60000) < this.wait ? true : false;
  }

  onSubmit() :void {
    const dataForm = this.myForm.getRawValue();

    if (this.checkWaitTime()) {
      this.eurResult = dataForm.usd / this.rates['USD'];
    } else {
      let payload = {
        params: {
          access_key: env.token,
          format: '1',
          symbols: 'USD'
        }
      }

      this.timeRequest = new Date();
      this.service.getExchangeValue(payload).then(data => {
        this.rates = data.rates;
        this.eurResult = dataForm.usd / this.rates['USD'];
      });
    }
  }

}
