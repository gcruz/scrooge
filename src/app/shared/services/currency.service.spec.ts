import { inject, async, TestBed } from '@angular/core/testing';
import { HttpTestingController } from '@angular/common/http/testing';
import { CurrencyService } from './currency.service';
import { HttpClientModule } from '@angular/common/http';

describe('CurrencyService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CurrencyService],
            imports: [ HttpClientModule ]
        });
    });

    it('should be created', inject(
        [CurrencyService],
        (service: CurrencyService) => {
          expect(service).toBeTruthy();
        }
    ));

    it('should get bad response', inject(
        [CurrencyService],
        (dataService: CurrencyService) => {
          const badResult = {
            success: false,
          };

          dataService.getExchangeValue({}).then(data => {
            switch (data) {
              case data:
                expect(data.success).toEqual(false);
            }
          });
        }
      )
    );
});
