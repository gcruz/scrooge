import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '@scrooge/env/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private http: HttpClient) {}

  /**
   * Get exchange result
   *
   * @returns {Promise<any>}
   */
  getExchangeValue(payload): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(
        `${env.api}`, payload
      )
      .subscribe(
        (response: any) => {
          resolve(response);
        },
        err => {
          resolve(false);
        }
      );
    });
  }
}
