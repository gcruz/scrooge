
import { CurrencyService } from '@scrooge/services/currency.service';

export const services: any[] = [
    CurrencyService
];

export * from '@scrooge/services/currency.service';
